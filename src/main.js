import Vue from 'vue';
import App from './App.vue';
import axios from 'axios';

import './assets/css/main.css';
import './assets/css/icons.css';

Vue.config.productionTip = false;

Vue.prototype.axios = axios;
axios.defaults.headers.common['Authorization'] = process.env.VUE_APP_API_KEY;
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;

new Vue({
  render: h => h(App),
}).$mount('#app');
